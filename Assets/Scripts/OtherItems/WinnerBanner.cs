﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WinnerBanner : MonoBehaviour {

    //called from animator
    private void OnAnimatorEnd()
    {
        StartDanceAnimation();
    }

    private void StartDanceAnimation()
    {
        StartCoroutine(DanceAnimation());
    }

    private IEnumerator DanceAnimation()
    {
        int factor = 1;
        while (true)
        {
            float duration = Random.Range(0.1f, 0.2f);
            transform.DORotate(new Vector3(0, 0, Random.Range(3 * factor, 6 * factor)), duration);
            yield return new WaitForSeconds(duration);
            factor *= -1;
        }
    }

}
