﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LoserBanner : MonoBehaviour {

    //called from animator
    private void OnAnimatorEnd()
    {
        StartShameAnimation();
    }

    private void StartShameAnimation()
    {
        StartCoroutine(ShameAnimation());
    }

    private IEnumerator ShameAnimation()
    {
        int factor = 1;
        float[] positions = new float[2];
        int index = 0;
        float duration = 4f;

        positions[0] = this.transform.position.x - 1f;
        positions[1] = this.transform.position.x + 1f;

        transform.DOMoveX(positions[index], duration/2f, false);
        yield return new WaitForSeconds(duration/2f);

        index = 1;

        while (true)
        {            
            transform.DOMoveX(positions[index], duration, false);
            yield return new WaitForSeconds(duration);

            index = index == 0 ? 1 : 0;
        }
    }
}
