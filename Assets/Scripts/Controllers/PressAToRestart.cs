﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressAToRestart : MonoBehaviour {

    private bool waitingForA = false;

    public void Activate()
    {
        waitingForA = true;
    }

    private void Update()
    {
        if(waitingForA)
        {
            if(IsAButtonUp())
            {
                waitingForA = false;
                gameObject.SetActive(false);
                SFX_Manager.Instance.PlayOneShot(SFX_Manager.Instance.uiChoose, 1);
                MpGameplayController.instance.RestartGameplay();
            }
        }
    }

    private bool IsAButtonUp()//is A down?
    {
        return Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire1_2");
    }

}
