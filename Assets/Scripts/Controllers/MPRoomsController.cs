﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MPRoomsController : MonoBehaviour {

    //References
    [SerializeField]
    private GameObject roomGameObject;
    [SerializeField]
    private Transform spawnerP1Transform;
    [SerializeField]
    private Transform spawnerP2Transform;
    [SerializeField]
    private MpSpawner spawnerP1;
    [SerializeField]
    private MpSpawner spawnerP2;
    [SerializeField]
    private SpriteRenderer imgRoomTag;
    [SerializeField]
    private RoomTypeAnim roomTypeAnim;
    [SerializeField]
    private Sprite[] sprRoomTags;
    [SerializeField]
    private Sprite sprEmpty;

    //Setup
    private Vector2 ROOM_STARTING_POS_OFFSET = new Vector2(0,-8.6f);

    //private references
    private Room p1Room;
    private Room p2Room;

    List<Room.ROOM_TYPE> availableRoomTypes;

    public event Action OnHideAndDestroyCurrentRooms;

    public bool IsRoomExitting { get; private set; }

    private void Start()
    {
        MpGameplayController.instance.OnGameplayStart += OnGameplayStart;
        MpGameplayController.instance.OnGameplayEnd += OnGameplayEnd;
        MpGameplayController.instance.OnRoomTimerStart += OnRoomTimerStart;
        MpGameplayController.instance.OnRoomTimerEnd += OnRoomTimerEnd;

        availableRoomTypes = new List<Room.ROOM_TYPE>{
            Room.ROOM_TYPE.KITCHEN,
            Room.ROOM_TYPE.BATH,
            Room.ROOM_TYPE.BED,
            Room.ROOM_TYPE.LIVING
        };
        IsRoomExitting = true;//lower volumes it not in gameplay
    }

    private void OnGameplayStart()
    {
        availableRoomTypes = new List<Room.ROOM_TYPE>{
            Room.ROOM_TYPE.KITCHEN,
            Room.ROOM_TYPE.BATH,
            Room.ROOM_TYPE.BED,
            Room.ROOM_TYPE.LIVING
        };

        roomTypeAnim.Show();
        //imgRoomTag.color = Color.white;
    }

    private void OnGameplayEnd()
    {
        roomTypeAnim.Hide();
        //imgRoomTag.color = new Color(0, 0, 0, 0);
        // Also destryo any remaining furniture
        HouseItem[] items = FindObjectsOfType<HouseItem>();
        foreach (HouseItem item in items)
        {
            Destroy(item.gameObject, 1);
            item.AnimateDestroy();
        }
    }

    private void OnRoomTimerStart()
    {
        SpawnNewRooms();
        spawnerP1.OnRoomReady();
        spawnerP2.OnRoomReady();
    }

    private void OnRoomTimerEnd()
    {
        //Check the score and store it!
        HideAndDestroyCurrentRooms();
    }

    public void SpawnNewRooms()
    {
        Debug.Log("SpawnNewRooms");
        Room.ROOM_TYPE roomType;

        roomType = GetNewRoomType();

        //p1 and p2
        p1Room = Instantiate(roomGameObject, (Vector2)spawnerP1Transform.position + ROOM_STARTING_POS_OFFSET, Quaternion.identity).GetComponent<Room>();
        p1Room.SetOwnerID(MpGameplayController.PlayerID.P1);
        p1Room.SetRoomType(roomType);
        p1Room.StartSpawnAnimation();
        p2Room = Instantiate(roomGameObject, (Vector2)spawnerP2Transform.position + ROOM_STARTING_POS_OFFSET, Quaternion.identity).GetComponent<Room>();
        p2Room.SetOwnerID(MpGameplayController.PlayerID.P2);
        p2Room.SetRoomType(roomType);
        p2Room.StartSpawnAnimation();

        if(MpGameplayController.instance.IsInGameplay)
        {
            roomTypeAnim.Show();
            //imgRoomTag.color = Color.white;
            imgRoomTag.sprite = sprRoomTags[(int) roomType];
        }
        IsRoomExitting = false;
    }

    public void HideAndDestroyCurrentRooms()
    {
        Debug.Log("HideAndDestroyCurrentRooms");
        p1Room.StartExitAnimation();
        p2Room.StartExitAnimation();
        if (OnHideAndDestroyCurrentRooms != null)
            OnHideAndDestroyCurrentRooms();
        SFX_Manager.Instance.PlayOneShot(SFX_Manager.Instance.changeRoom, 1);

        roomTypeAnim.Hide();
        //imgRoomTag.color = new Color(0, 0, 0, 0);

        IsRoomExitting = true;
    }

    private Room.ROOM_TYPE GetNewRoomType()
    {
        Room.ROOM_TYPE result;

        int randomIndex = UnityEngine.Random.Range(0, availableRoomTypes.Count);

        result = availableRoomTypes[randomIndex];

        availableRoomTypes.RemoveAt(randomIndex);

        return result;
    }

    public Room.ROOM_TYPE GetCurrentRoomType()
    {
        return p1Room.roomType;
    }

}
