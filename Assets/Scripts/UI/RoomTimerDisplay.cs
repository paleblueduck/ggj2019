﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomTimerDisplay : MonoBehaviour {

    [SerializeField]
    private Image timerImage;

    private void Start()
    {
        MpGameplayController.instance.OnRoomTimerStart += OnRoomTimerStart;
        MpGameplayController.instance.OnRoomTimerTick+= OnRoomTimerTick;
        MpGameplayController.instance.OnRoomTimerEnd += OnRoomTimerEnd;
    }

    private void OnRoomTimerStart()
    {
        timerImage.enabled = true;
        timerImage.fillAmount = 1;
    }

    private void OnRoomTimerTick(float normalizedProgress)//sends normalized progress
    {
        timerImage.fillAmount = normalizedProgress;
    }

    private void OnRoomTimerEnd()
    {
        timerImage.enabled = false;
    }

}
