﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(FallingObject))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public sealed class HouseItem : MonoBehaviour {

    //references
    [SerializeField]
    private Collider2D m_collider2D;
    [SerializeField]
    private Rigidbody2D m_rigidbody2D;
    private SpriteRenderer spriteRenderer;

    private MpGameplayController.PlayerID m_ownerID;
    [SerializeField]
    private FURNITURE_TYPE furnitureType;

    [SerializeField]
    private List<Room.ROOM_TYPE> compatibleRooms;

    bool m_hasCollided = false;
    bool m_hasStopped = false;
    bool m_selectionIncompatible = false;

    private float VELOCITY_FOR_HIGH_SOUND = 17;
    private float COLLISION_VOLUME_MULTIPLIER = 0.5f;
    private float ROOM_EXITTING_VOLUME_MULTIPLIER = 0.2f;

    private const float AFTER_RELEASED_IMMUNITY = 0.2f;

    public enum FURNITURE_TYPE
    {
        STOVE = 0,
        FRIDGE,
        PAN,
        TABLE,
        POT,
        FRUIT,
        PLANT,
        DRAWERS,
        TOILET,
        MIRROR,
        FAUCET,
        CLOTHS,
        TUB,
        BIN,
        BED,
        COUNTER,
        LAMP,
        PHOTO,
        STEP,//Taburetito
        SOFA,
        COUCH,
        COFFEE,
        BOOK,
        TV,
        TOASTER,
        TP,
        TRASH,
        CAT,
        SHOVEL,
        SKULL,
        POOP
    }

    //private state
    public enum HouseItemState { BeforeSelected, PickedUp, Released };
    private HouseItemState m_state;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Start()
    {
        
    }

    public void StartAsBeforeSelected()
    {
        m_state = HouseItemState.BeforeSelected;
        m_rigidbody2D.isKinematic = true;
        m_collider2D.enabled = false;
        StartDanceAnimation();
    }

    public void OnPickedUp()
    {
        m_state = HouseItemState.PickedUp;
        m_rigidbody2D.isKinematic = true;
        m_collider2D.enabled = false;
        StopDanceAnimation();
        StartAutomaticRotate();

        if (!compatibleRooms.Contains(MpGameplayController.instance.mpRoomsController.GetCurrentRoomType()))
        {
            m_selectionIncompatible = true;
            SFX_Manager.Instance.PlayOneShot(SFX_Manager.Instance.sadParticles, 1);
        }
    }

    public void OnReleased()
    {
        m_state = HouseItemState.Released;
        m_rigidbody2D.isKinematic = false;
        StartCoroutine(OnReleasedImmunity());
    }

    private IEnumerator OnReleasedImmunity()
    {
        yield return new WaitForSeconds(AFTER_RELEASED_IMMUNITY);
        m_collider2D.enabled = true;
    }

    public void SetPosition(Vector2 pos)
    {
        if(m_state == HouseItemState.PickedUp)
        {
            m_rigidbody2D.MovePosition(pos);
        }
    }

    public void SetVelocity(Vector2 vel)
    {
        if (m_state == HouseItemState.PickedUp)
        {
            m_rigidbody2D.velocity = vel;
        }
    }
    
    public bool IsBeingPickedUp()
    {
        return m_state == HouseItemState.PickedUp;
    }

    public HouseItemState GetState()
    {
        return m_state;
    }

    private void StartAutomaticRotate()
    {
        m_rigidbody2D.angularVelocity = HouseItemsConstants.ROTATION_VELOCITY;
    }

    Tween m_danceTween;
    Coroutine m_danceCoroutine;
    private void StartDanceAnimation()
    {
        m_danceCoroutine = StartCoroutine(DanceAnimation());
    }

    private IEnumerator DanceAnimation()
    {
        int factor = 1;
        while(true)
        {
            float duration = Random.Range(0.1f, 0.2f);
            m_danceTween = transform.DORotate(new Vector3(0, 0, Random.Range(5*factor, 14*factor)), duration);
            yield return new WaitForSeconds(duration);
            factor *= -1;
        }
    }

    private void StopDanceAnimation()
    {
        StopCoroutine(m_danceCoroutine);
        m_danceTween.Kill();
        m_danceTween = null;
    }

    public MpGameplayController.PlayerID GetOwnerID()
    {
        return m_ownerID;
    }

    public void SetOwnerID(MpGameplayController.PlayerID owner)
    {
        this.m_ownerID = owner;
    }

    public FURNITURE_TYPE GetFurnitureType()
    {
        return furnitureType;
    }

    public void AnimateDestroy()
    {
        spriteRenderer.DOFade(0, 0.3f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        HandleCollision(other);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        float normalizedVolume;
        //if(col.relativeVelocity.magnitude>0.2f)
        {
            if (col.relativeVelocity.magnitude < VELOCITY_FOR_HIGH_SOUND)
            {
                normalizedVolume = col.relativeVelocity.magnitude / VELOCITY_FOR_HIGH_SOUND;
            }
            else
            {
                normalizedVolume = 1;
            }
            normalizedVolume = normalizedVolume * COLLISION_VOLUME_MULTIPLIER * ((MpGameplayController.instance.mpRoomsController.IsRoomExitting || MpGameplayController.instance.IsShowingGameScore) ?ROOM_EXITTING_VOLUME_MULTIPLIER:1);
            //Debug.Log("Collision vel:" + col.relativeVelocity);
            SFX_Manager.Instance.PlayOneShot(SFX_Manager.Instance.fallingFurniture, normalizedVolume);
        }
        HandleCollision(col.otherCollider);
    }

    private void HandleCollision(Collider2D other)
    {
        if (other.CompareTag("DeathZone") && MpGameplayController.instance.IsInGameplay)
        {
            GameEventsController.instance.RaiseEvent(GameEventsController.GameEvent.FALLING_OBJECT_OUTTA_BOUNDS, this.gameObject);
            AnimateDestroy();
            Destroy(gameObject, 1);
        }
        else if (!other.CompareTag("Wall") && !m_hasCollided)
        {
            m_hasCollided = true;
            GameEventsController.instance.RaiseEvent(GameEventsController.GameEvent.FALLING_OBJECT_TOUCHDOWN, this.gameObject);
            StartCoroutine(checkForStop(gameObject.transform));
        }
    }

    IEnumerator checkForStop(Transform _transform)
    {
        float velocityThreshold = 0.2f;
        yield return Yielders.EndOfFrame;
        while (!m_hasStopped)
        {
            m_hasStopped = m_rigidbody2D.angularVelocity <= velocityThreshold && m_rigidbody2D.velocity.x <= velocityThreshold && m_rigidbody2D.velocity.y <= velocityThreshold;
            yield return Yielders.WaitForSeconds(0.1f);

            if (m_hasStopped && (m_rigidbody2D.angularVelocity > velocityThreshold && m_rigidbody2D.velocity.x > velocityThreshold && m_rigidbody2D.velocity.y > velocityThreshold))
            {
                m_hasStopped = false;
            }
        }
        if (!MpGameplayController.instance.IsInGameplay)
        {
            Instantiate(Resources.Load("ParticlesGood"), transform.position, Quaternion.identity);
        }
        else if (m_selectionIncompatible)
        {
            Instantiate(Resources.Load("ParticlesGood"), transform.position, Quaternion.identity);
        }
        else
        {
            float score = ScoreValidator.Instance.fGetScoreFromAngle(_transform, m_ownerID);
            if (score < 0.3f)
            {
                Instantiate(Resources.Load("ParticlesBad"), transform.position, Quaternion.identity);
            }
            else //if (score > 0.8f)
            {
                Instantiate(Resources.Load("ParticlesGood"), transform.position, Quaternion.identity);
            }
        }
        Debug.Log(gameObject.name+" stopped");
    }
}
