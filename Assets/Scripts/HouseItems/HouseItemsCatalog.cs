﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HouseItemsCatalog : MonoBehaviour {

    [SerializeField]
    private HouseItem[] m_kitchenItems;

    [SerializeField]
    private HouseItem[] m_bathroomItems;

    [SerializeField]
    private HouseItem[] m_bedroomItems;

    [SerializeField]
    private HouseItem[] m_livingroomItems;

    [SerializeField]
    private HouseItem[] m_badItems;

    public HouseItem GetRandomHouseItem(/*send category later or something */)
    {//to be implemented later so that it sorts by type
        return m_kitchenItems[Random.Range(0,m_kitchenItems.Length)];
    }

    public HouseItem[] GetRandomHouseItems(int ammount/*send category later or something */)
    {//to be implemented later so that it sorts by type
        HouseItem[] temp = new HouseItem[ammount];
        for (int x = 0; x < ammount; x++)
        {
            temp[x] = m_kitchenItems[Random.Range(0, m_kitchenItems.Length)];
        }
        return temp;
    }

    public HouseItem GetRandomHouseItemOfType(Room.ROOM_TYPE type)
    {
        HouseItem result = null;

        switch (type)
        {
            case Room.ROOM_TYPE.KITCHEN:
                result = m_kitchenItems[Random.Range(0, m_kitchenItems.Length)];
                break;
            case Room.ROOM_TYPE.BATH:
                result = m_bathroomItems[Random.Range(0, m_bathroomItems.Length)];
                break;
            case Room.ROOM_TYPE.BED:
                result = m_bedroomItems[Random.Range(0, m_bedroomItems.Length)];
                break;
            case Room.ROOM_TYPE.LIVING:
                result = m_livingroomItems[Random.Range(0, m_livingroomItems.Length)];
                break;
        }

        return result;
    }

    public HouseItem GetRandomHouseItemExcludingOfType(Room.ROOM_TYPE type)
    {

        List<HouseItem> items = new List<HouseItem>();

        if(type != Room.ROOM_TYPE.KITCHEN)
        {
            items.AddRange(m_kitchenItems);
        }

        if (type != Room.ROOM_TYPE.BATH)
        {
            items.AddRange(m_bathroomItems);
        }

        if (type != Room.ROOM_TYPE.BED)
        {
            items.AddRange(m_bedroomItems);
        }

        if (type != Room.ROOM_TYPE.LIVING)
        {
            items.AddRange(m_livingroomItems);
        }

        items.AddRange(m_badItems);

        int randomIndex = Random.Range(0, items.Count);

        return items[randomIndex];
    }

    public HouseItem GetItemOfType(HouseItem.FURNITURE_TYPE type)
    {
    	HouseItem result = null;

    	switch(type)
    	{
	        case HouseItem.FURNITURE_TYPE.STOVE:
	        case HouseItem.FURNITURE_TYPE.FRIDGE:
	        case HouseItem.FURNITURE_TYPE.PAN:
	        case HouseItem.FURNITURE_TYPE.TABLE:
	        case HouseItem.FURNITURE_TYPE.POT:
	        case HouseItem.FURNITURE_TYPE.FRUIT:
	        case HouseItem.FURNITURE_TYPE.PLANT:
	        case HouseItem.FURNITURE_TYPE.DRAWERS:
	        	result = m_kitchenItems[(int) type];
	        break;

	        case HouseItem.FURNITURE_TYPE.TOILET:
	        case HouseItem.FURNITURE_TYPE.MIRROR:
	        case HouseItem.FURNITURE_TYPE.FAUCET:
	        case HouseItem.FURNITURE_TYPE.CLOTHS:
	        case HouseItem.FURNITURE_TYPE.TUB:
	        case HouseItem.FURNITURE_TYPE.BIN:
	        	result = m_kitchenItems[(int) type - m_kitchenItems.Length];
	        break;
    	}

    	return result;
    }

}
