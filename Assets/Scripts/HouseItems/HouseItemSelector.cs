﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public sealed class HouseItemSelector : MonoBehaviour {

    //Display to select stuff
    [SerializeField]
    private GameObject selector;

    //make an array later or something
    private HouseItem[] m_items;

    private HouseItem m_currentSelection;
    private int m_currentSelectionIndex;

    private const float ITEMS_SEPARATION = 3;
    private const float ITEMS_FALL_ANIM_OFFSET = 6.0F;

    private bool canSelect = false;

    private void Start()
    {
        selector.SetActive(false);
        MpGameplayController.instance.OnGameplayEnd += OnGameplayEnd;
    }

    //will later receive more than one dude.
    public void SetHouseItemsToSelectFrom(HouseItem[] items)
    {
        DeleteOldDudes();
        //new stuff
        m_items = items;
        StopAllCoroutines();
        canSelect = false;
        DisplayItemsToSelectFrom();
    }

    public void OnGameplayEnd()
    {
        DeleteOldDudes();
        StopAllCoroutines();
        canSelect = false;
        m_currentSelection = null;
    }

    public HouseItem GetSelectedHouseItem()
    {
        if (!canSelect)
            return null;
        DeleteOldDudes();
        HouseItem temp = m_currentSelection;
        m_currentSelection = null;
        return temp;
    }

    private void DeleteOldDudes()
    {
        //delete old duds
        if (m_items != null)
        {
            for (int x = 0; x < m_items.Length; x++)
            {
                if (x != m_currentSelectionIndex)
                {
                    m_items[x].AnimateDestroy();
                    Destroy(m_items[x].gameObject, 1);
                }
            }
            m_items = null;
            selector.SetActive(false);
        }
    }

    private void DisplayItemsToSelectFrom()
    {//based on the offset and current pos and bla bla. HARDOCDED TO 3 BECAUSE FUCK YOU
        Vector2 basePos = m_items[1].transform.position;
        m_items[0].transform.position = basePos + (Vector2.left * ITEMS_SEPARATION);
        m_items[2].transform.position = basePos + (Vector2.right * ITEMS_SEPARATION);
        //Animate!

        //Target Positions
        Vector2 _0Target = m_items[0].transform.position;
        Vector2 _1Target = m_items[1].transform.position;
        Vector2 _2Target = m_items[2].transform.position;
        //Now position above target and animate!
        m_items[0].transform.position = _0Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        m_items[1].transform.position = _1Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        m_items[2].transform.position = _2Target + Vector2.up * ITEMS_FALL_ANIM_OFFSET;
        //aNIMATE
        float time0 = Random.Range(0.3f, 0.9f);
        float time1 = Random.Range(0.3f, 0.9f);
        float time2 = Random.Range(0.3f, 0.9f);
        m_items[0].transform.DOMove(_0Target, time0);
        m_items[1].transform.DOMove(_1Target, time1);
        m_items[2].transform.DOMove(_2Target, time2);
        //get the biggest
        float maxTime = Mathf.Max(time0, time1, time2);
        StartCoroutine(WaitForAnimationsToFinish(maxTime));
    }

    private IEnumerator WaitForAnimationsToFinish(float time)
    {
        yield return new WaitForSeconds(time);
        canSelect = true;
        SelectHouseItem(1, false);
    }

    public void OnHorizontalAxisPressed(float rawAxis)
    {
        if(m_items != null && canSelect)
        {
            if (rawAxis > 0)//right
            {
                if(m_currentSelectionIndex< m_items.Length-1)
                {
                    SelectHouseItem(m_currentSelectionIndex + 1);
                }
            }
            else if(rawAxis < 0)//left
            {
                if (m_currentSelectionIndex > 0)
                {
                    SelectHouseItem(m_currentSelectionIndex - 1);
                }
            }
        }
    }

    private void SelectHouseItem(int idx, bool playSound = true)
    {
        if(canSelect)
        {
            Debug.Log("SelectHouseItem: "+ idx);
            m_currentSelectionIndex = idx;
            selector.SetActive(true);
            m_currentSelection = m_items[idx];
            selector.transform.position = m_currentSelection.transform.position;
            if (playSound)
                SFX_Manager.Instance.PlayOneShot(SFX_Manager.Instance.uiChoose, 1);
        }
    }

}
