﻿using UnityEngine;

public class FurnitureProperties {
	public HouseItem.FURNITURE_TYPE houseItemId;
    public Vector3 position;
    public Quaternion rotation;        

    public FurnitureProperties(HouseItem.FURNITURE_TYPE _id, Vector3 _position, Quaternion _rotation)
    {
    	houseItemId = _id;
    	position = _position;
    	rotation = _rotation;  
    }
}
