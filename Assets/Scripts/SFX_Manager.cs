﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX_Manager : MonoBehaviour {

    public static SFX_Manager Instance;

    public AudioClip fallingFurniture;
    public AudioClip changeRoom;
    public AudioClip uiChoose;
    public AudioClip sadParticles;

    private void Awake()
    {
        Instance = this;        
    }

    public AudioSource audioSource;

    public void PlayOneShot(AudioClip clip, float volume)
    {
        audioSource.PlayOneShot(clip, volume);
    }

}
